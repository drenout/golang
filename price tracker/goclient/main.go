package main

import (
	"fmt"
	"log"
	"strconv"
	"time"

	client "github.com/influxdata/influxdb1-client/v2"
)

type priceChecker interface {
	getPrices() []aptData
	getDbName() string
}

type aptData map[string]string

type apartments struct {
	sites []string
}

func insertQuery(a aptData, dbName string) (client.BatchPoints, error) {
	// Create a new point batch
	bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  dbName,
		Precision: "s",
	})
	fields := make(map[string]interface{})
	// Create a point and add to batch
	tags := map[string]string{"Apartment": a["apt"]}

	minPrice, err := strconv.Atoi(a["minPrice"])
	if err == nil {
		fields["minPrice"] = minPrice
	} else {
		log.Println("Error while converting minPrice.")
	}

	maxPrice, err := strconv.Atoi(a["maxPrice"])
	if err == nil {
		fields["maxPrice"] = maxPrice
	} else {
		log.Println("Error while converting maxPrice.")
	}

	measurement := fmt.Sprintf("apt_%s", a["apt"])
	pt, err := client.NewPoint(measurement, tags, fields, time.Now())
	if err != nil {
		log.Println("Error: ", err.Error())
		return nil, err
	}
	log.Println("Adding batch point for", measurement)
	bp.AddPoint(pt)
	return bp, nil
}

func insertAptsData(p priceChecker) {
	con, err := client.NewHTTPClient(client.HTTPConfig{
		Addr: "http://influxdb:8086",
	})

	if err != nil {
		log.Println("Error creating InfluxDB Client: ", err.Error())
	}
	defer con.Close()

	details := p.getPrices()
	dbName := p.getDbName()

	for _, apt := range details {
		bp, insertErr := insertQuery(apt, dbName)
		if insertErr == nil {
			// Write the batch
			con.Write(bp)
		}
	}
}

func main() {
	// sleep to wait db initialisation during docker-compose up
	// Could be improved by polling the actual db status
	log.Println("Waiting for the influxdb initialisation")
	time.Sleep(20 * time.Second)
	for {
		log.Println("Getting prices for the Sofi apartments")
		insertAptsData(sofiRooms)
		time.Sleep(1 * time.Hour)
	}
}
