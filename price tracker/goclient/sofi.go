package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"
	"sync"
)

// defined new type to use the same interfaces for different apartments, e.g. getPrices
type sofi apartments

var sofiRooms = sofi{
	sites: []string{
		// "https://www.sofiriverviewpark.com/availableunits.aspx?myOlePropertyId=234323&floorPlans=2030010",
		// "https://www.sofiriverviewpark.com/availableunits.aspx?myOlePropertyId=234323&floorPlans=2030011",
		"https://www.sofiriverviewpark.com/availableunits.aspx?myOlePropertyId=234323",
	},
}

func parseSofi(link string, wg *sync.WaitGroup, queue chan aptData) {
	defer wg.Done()
	resp, err := http.Get(link)
	if err != nil {
		log.Fatalln("Error:", err)
	}
	defer resp.Body.Close()

	// Read response data in to memory
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("Error reading HTTP body. ", err)
	}

	// Create a regular expression to find apartments data
	re := regexp.MustCompile(`'Apartment'>#(?P<Apt>.*?)<.*?Ft\.'>(?P<Feet>.*?)<.*?'Rent'>\$(?P<MinPrice>.*?)-\$(?P<MaxPrice>.*?)<.*?'Date Available'><.*?>(?P<Date>.*?)<`)
	responseData := re.FindAllStringSubmatch(string(body), -1)
	if responseData == nil {
		log.Println("No matches for the Sofi apartments.")
		return
	}

	// Send apartments data to the channel
	for _, match := range responseData {
		queue <- aptData{
			"apt":  match[1],
			"feet": match[2],
			// remove commas from prices, i.e. 3,000 will be converted to 3000
			// it requires to insert the data to influxdb
			"minPrice": strings.ReplaceAll(match[3], ",", ""),
			"maxPrice": strings.ReplaceAll(match[4], ",", ""),
			"date":     match[5],
		}
	}
}

func (s sofi) getPrices() []aptData {
	var wg sync.WaitGroup
	var sofiApts = []aptData{}
	queue := make(chan aptData)
	done := make(chan bool)

	for _, link := range sofiRooms.sites {
		wg.Add(1)
		go parseSofi(link, &wg, queue)
	}

	// Create control goroutine to send done when all the other goroutines are finished
	go func(wg *sync.WaitGroup) {
		wg.Wait()
		done <- true
	}(&wg)

	// Read data from channel queue until all the goroutines are not done
	for {
		select {
		case elem := <-queue:
			sofiApts = append(sofiApts, elem)
		case <-done:
			return sofiApts
		}
	}
}

func (s sofi) getDbName() string {
	return "sofi"
}
