# Apartments price tracker

## How to
* Clone the project
* Run `docker-compose up --build -d`
* Login to grafana (http://localhost:3000) with admin:grafana. Price tracker dashboards are automatically created and avaolable via Dashboards -> Manage menu

## Services description
 - influxdb - Influxdb with precreated databases to store apartments prices
 - goclient - web scrapper which fetches prices and store it in the influxdb
 - grafana - grafana with automatically created datasources and dashboards for price tracking