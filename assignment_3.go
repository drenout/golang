package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	fileNames := os.Args[1:]
	if len(fileNames) == 0 {
		fmt.Println("No file was provided")
		os.Exit(1)
	}
	file, err := os.Open(fileNames[0])
	if err != nil {
		fmt.Println("Cannot open the file", fileNames[0])
		os.Exit(1)
	}
	fmt.Println("Content of the", fileNames[0])
	io.Copy(os.Stdout, file)

}
